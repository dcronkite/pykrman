# Python Intelligent Character Recognition Manager

## About
Manage ICR/OCR on a variety of inputs, primarily tiff and scanned pdfs.

## License
MIT: https://dcronkite.mit-license.org/
